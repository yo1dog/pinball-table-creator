package Tools;
public class LineTool extends Tool
{
	public LineTool()
	{
		c = java.awt.Color.blue;
	}
	
	public void Action(int x2, int y2)
	{
		if (x1 != x2 || y1 != y2)
			Default.Table.AddShape(new Shapes.Line(x1, y1, x2, y2));
	}

	public void Draw(int x2, int y2, java.awt.Graphics g)
	{
		g.setColor(c);
		g.drawLine(x1, y1, x2, y2);
	}
}
