package Tools;
import Shapes.Shape;
public class SelectTool extends Tool
{
	public SelectTool()
	{
		c = java.awt.Color.white;
	}
	
	public void Action(int x2, int y2)
	{
		Shape[] shapes = Default.Table.GetRectCollision(Math.min(x1, x2) - 1, Math.min(y1, y2) - 1, Math.max(x1, x2) + 1, Math.max(y1, y2) + 1);
		
		for (int i = 0; i < shapes.length; i++)
			shapes[i].Select();
	}

	public void Draw(int x2, int y2, java.awt.Graphics g)
	{
		g.setColor(c);
		g.drawRect(Math.min(x1, x2), Math.min(y1, y2), Math.abs(x2 - x1), Math.abs(y2 - y1));
	}
}
