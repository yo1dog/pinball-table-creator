package Tools;
public class CircleTool extends Tool
{
	public CircleTool()
	{
		c = java.awt.Color.blue;
	}
	
	public void Action(int x2, int y2)
	{
		if (x1 != x2 || y1 != y2)
			Default.Table.AddShape(new Shapes.Circle(x1, y1, (int)Math.sqrt((x2 - x1)*(x2 - x1) + (y2 - y1)*(y2 - y1))));
	}

	public void Draw(int x2, int y2, java.awt.Graphics g)
	{
		g.setColor(c);
		int r = (int)Math.sqrt((x2 - x1)*(x2 - x1) + (y2 - y1)*(y2 - y1));
		g.drawOval(x1 - r, y1 - r, r*2, r*2);
	}
}
