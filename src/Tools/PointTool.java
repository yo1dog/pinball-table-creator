package Tools;
public class PointTool extends Tool
{
	public PointTool()
	{
		c = java.awt.Color.red;
	}
	
	public void Action(int x, int y)
	{
		Default.Table.AddShape(new Shapes.Point(x, y));
	}

	public void Draw(int x, int y, java.awt.Graphics g)
	{
		g.setColor(c);
		g.drawLine(x - 2, y - 2, x + 2, y + 2);
		g.drawLine(x + 2, y - 2, x - 2, y + 2);
	}
}
