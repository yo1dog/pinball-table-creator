package Tools;
public abstract class Tool
{
	protected java.awt.Color c;
	protected int x1, y1;
	
	public final void Start(int x, int y)
	{
		x1 = x;
		y1 = y;
	}
	
	public abstract void Action(int x2, int y2);
	public abstract void Draw(int x2, int y2, java.awt.Graphics g);
}