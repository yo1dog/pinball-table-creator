package Default;
public final class G
{
	public static void drawArrow(int x1, int y1, int x2, int y2, int s, java.awt.Graphics g)
	{
		g.drawLine(x1, y1, x2, y2);
		
		float t = (float)Math.atan2(y1 - y2, x1 - x2);
		g.drawLine(x2, y2, x2 + (int)(Math.cos(t + Math.PI/4)*s), y2 + (int)(Math.sin(t + Math.PI/4)*s));
		g.drawLine(x2, y2, x2 + (int)(Math.cos(t - Math.PI/4)*s), y2 + (int)(Math.sin(t - Math.PI/4)*s));
	}
}
