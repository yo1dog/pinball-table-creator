package Default;
import java.util.ArrayList;
import Shapes.Shape;

public final class Table
{
	public static final double defaultDamping  = 0.4d;      // default damping (bounciness) of collisions
	public static final double defaultFriction = 0.005d;    // default friction (0 - 1)
	
	private static ArrayList<Shape> shapes = new ArrayList<Shape>();
	
	//*********************************************************
	// Add Shape
	//
	//  adds a shape to the table
	//*********************************************************
	public static void AddShape(Shape s)
	{
		shapes.add(s);
	}
	
	
	//*********************************************************
	// Get Closest Point
	//
	//  gets the point on a shape closest to a point on the table
	//*********************************************************
	public static java.awt.Point GetClosestPoint(int x, int y)
	{
		int s = shapes.size();
		if (s == 0)
			return new java.awt.Point(x, y);
		
		java.awt.Point pMin = shapes.get(0).GetClosestPoint(x, y);
		double dMin = Math.sqrt((y - pMin.y)*(y - pMin.y)+ (x - pMin.x)*(x - pMin.x));
		
		for (int i = 1; i < s; i++)
		{
			java.awt.Point p = shapes.get(i).GetClosestPoint(x, y);
			double d = Math.sqrt((y - p.y)*(y - p.y)+ (x - p.x)*(x - p.x));
			
			if (d < dMin)
			{
				dMin = d;
				pMin = p;
			}
		}
		
		return pMin;
	}
	
	
	//*********************************************************
	// Get Rectangle Collision 
	//
	//  returns an array of shapes that collide with a rectangle
	//*********************************************************
	public static Shape[] GetRectCollision(int x1, int y1, int x2, int y2)
	{
		System.out.println(x1 + ", " + y1 + ", " + x2 + ", " + y2);
		
		int s = shapes.size();
		if (s == 0)
			return null;
		
		ArrayList<Shape> cShapes = new ArrayList<Shape>();
		for (int i = 0; i < s; i++)
		{
			Shape shape = shapes.get(i);
			if (shape.GetRectCollision(x1, y1, x2, y2))
				cShapes.add(shape);
		}
		
		Shape[] rtrn = new Shape[cShapes.size()];
		cShapes.toArray(rtrn);
		cShapes.clear();
		cShapes = null;
		
		return rtrn;
	}
		
	
	//*********************************************************
	// Draw
	//
	//  draws the shapes on the table
	//*********************************************************
	public static void Draw(java.awt.Graphics g)
	{
		for (int i = 0; i < shapes.size(); i++)
			shapes.get(i).Draw(g);
	}
}
