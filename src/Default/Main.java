package Default;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Event;
import java.awt.event.KeyEvent;
import java.applet.Applet;
import Tools.Tool;

public class Main extends Applet implements Runnable
{
	//*********************************************************
	// Create Variables and Constants
	//
	//*********************************************************
	
	private static final long serialVersionUID = -6459221710559842746L;
	public static final int  width =  601; // width of the room
	public static final int  height = 901; // height of the room
	private final int speed = 12;          // ms between frames
	
	public static int mouseX, mouseY;
	
	private Thread th;     // thread
	private Image dbImage; // game image
	private Graphics dbg;  // graphics object
	
	private boolean keyState[] = new boolean[127];
	private boolean mouseState[] = new boolean[3];
	
	private Tool SelectTool = new Tools.SelectTool();
	private Tool LineTool = new Tools.LineTool();
	private Tool PointTool = new Tools.PointTool();
	private Tool CircleTool = new Tools.CircleTool();
	private Tool tool = LineTool;
	private boolean toolActive = false;
	
	// Grid
	private int gridSize = 32;
	private boolean gridSnap = true;
	private boolean gridDraw = true;
	private Color gridColor = Color.darkGray;
	
	// Snap to points
	private boolean pointSnap = false;
	
	
	//*********************************************************
	// Initiate Method
	//
	//*********************************************************
	public void init()
	{
		setBackground(Color.black);
		this.setMinimumSize(new Dimension(width, height));
		this.setMaximumSize(new Dimension(width, height));
		this.setPreferredSize(new Dimension(width, height));
		this.setSize(width, height);
	}
	
	//*********************************************************
	// Thread Methods
	//
	//*********************************************************
	// start thread
	public void start()
	{
		th = new Thread(this);
		th.start();
	}
	
	// stop thread
	@SuppressWarnings("deprecation")
	public void stop()
	{
		th.stop();
	}
	
	
	//*********************************************************
	// Mouse Methods
	//
	//*********************************************************
	// mouse is pressed
	public boolean mouseDown(Event e, int x, int y)
	{
		mouseX = x;
		mouseY = y;
		mouseState[0] = true;
		
		if (pointSnap)
		{
			java.awt.Point p = Table.GetClosestPoint(x, y);
			tool.Start(p.x, p.y);
		}
		else if (gridSnap)
			tool.Start((int)((float)x / gridSize + 0.5f) * gridSize, (int)((float)y / gridSize + 0.5f) * gridSize);
		else
			tool.Start(x, y);
		
		toolActive = true;
		
		return true;
	}
	
	// mouse is released
	public boolean mouseUp(Event e, int x, int y)
	{
		mouseX = x;
		mouseY = y;
		mouseState[0] = false;
		
		if (pointSnap)
		{
			java.awt.Point p = Table.GetClosestPoint(x, y);
			tool.Action(p.x, p.y);
		}
		else if (gridSnap)
			tool.Action((int)((float)x / gridSize + 0.5f) * gridSize, (int)((float)y / gridSize + 0.5f) * gridSize);
		else
			tool.Action(x, y);
		toolActive = false;
		
		return true;
	}
	
	// mouse moves
	public boolean mouseMove(Event e, int x, int y)
	{
		mouseX = x;
		mouseY = y;
		
		return true;
	}
	
	public boolean mouseDrag(Event e, int x, int y)
	{
		mouseX = x;
		mouseY = y;
		
		return true;
	}
	
	
	//*********************************************************
	// Keyboard Methods
	//
	//*********************************************************
	// key is pressed
	public boolean keyDown(Event e, int key)
	{
		if (key < keyState.length)
			keyState[key] = true;
		
		// grid settings
		switch (key)
		{
			case 103:	// g
				gridSnap = !gridSnap;
				break;
			case 71:	// G
				gridDraw = !gridDraw;
				break;
			case 115:	// s
				pointSnap = !pointSnap;
		}
		
		// change tools
		if (!toolActive)
		{
			switch (key)
			{	
				case 108:	// l
					tool = LineTool;
					break;
				
				case 99:	// c
					tool = CircleTool;
					break;
				
				case 112:	// p
					tool = PointTool;
					break;
				
				case 32:	// space
					tool = SelectTool;
					break;
			}
		}
		
		return true;
	}
	
	// key is release
	public boolean keyUp(Event e, int key)
	{
		if (key < keyState.length)
			keyState[key] = false;
	
		return true;
	}
	
	//*********************************************************
	// Main Loop Method
	//
	//*********************************************************
	public void run()
	{
		// set thread priority
		th.setPriority(Thread.MAX_PRIORITY);
		
		// start Loop
		while (true)
		{
			
			
			//------------------------------------------------
			// Post Operations
			//------------------------------------------------
			
			// re-draw the scene
			repaint();
			
			// operate the thread
			try
			{
				Thread.sleep(speed);
			}
			catch (InterruptedException ex)
			{
			}
			
			Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
		}
	}
	
	
	//*********************************************************
	// Draw Game
	//
	//*********************************************************
	public void paint(Graphics g)
	{
		// draw grid
		if (gridDraw)
		{
			g.setColor(gridColor);
			
			for (int x = 0; x < width; x = x += gridSize)
				g.drawLine(x, 0, x, height);
			for (int y = 0; y < height; y += gridSize)
				g.drawLine(0, y, width, y);
		}
		
		// draw table
		Table.Draw(g);
		
		// draw tool
		if (toolActive)
		{
			if (pointSnap)
			{
				java.awt.Point p = Table.GetClosestPoint(mouseX, mouseY);
				tool.Draw(p.x, p.y, g);
			}
			else if (gridSnap)
				tool.Draw((int)((float)mouseX / gridSize + 0.5f) * gridSize, (int)((float)mouseY / gridSize + 0.5f) * gridSize, g);
			else
				tool.Draw(mouseX, mouseY, g);
		}
		
		// draw pointer
		else
		{
			g.setColor(Color.white);
			
			if (pointSnap)
			{
				java.awt.Point p = Table.GetClosestPoint(mouseX, mouseY);
				g.drawLine(p.x - 10, p.y, p.x + 10, p.y);
				g.drawLine(p.x, p.y - 10, p.x, p.y + 10);
			}
			else if (gridSnap)
			{
				int x = (int)((float)mouseX / gridSize + 0.5f) * gridSize;
				int y = (int)((float)mouseY / gridSize + 0.5f) * gridSize;
				
				g.drawLine(x - 10, y, x + 10, y);
				g.drawLine(x, y - 10, x, y + 10);
			}
			else
			{
				g.drawLine(mouseX - 10, mouseY, mouseX + 10, mouseY);
				g.drawLine(mouseX, mouseY - 10, mouseX, mouseY + 10);
			}
		}
			
	}
	
	
	//*********************************************************
	// Update Screen
	//
	//*********************************************************
	public void update(Graphics g)
	{
		if (dbImage == null)
		{
			dbImage = createImage(width, height);
			dbg = dbImage.getGraphics();
		}
		
		dbg.setColor(Color.black);
		dbg.fillRect(0, 0, width, height);
		
		dbg.setColor(getForeground());
		paint(dbg);
		
		g.drawImage(dbImage, 0, 0, this);
	}
}