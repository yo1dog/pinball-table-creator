package Shapes;
public final class Point extends Shape
{
	int x, y;
	double friction;
	double damping;
	
	public Point(int x, int y)
	{
		this.x = x;
		this.y = y;
		
		c = java.awt.Color.red;
	}
	
	public java.awt.Point GetClosestPoint(int bx, int by)
	{
		return new java.awt.Point(x, y);
	}
	
	public boolean GetRectCollision(int rx1, int ry1, int rx2, int ry2)
	{
		return (x > rx1 && x < rx2 && y > ry1 && y < ry2);
	}
	
	public void Draw(java.awt.Graphics g)
	{
		if (selected)
			g.setColor(java.awt.Color.green);
		else
			g.setColor(c);
		
		g.drawLine(x - 2, y - 2, x + 2, y + 2);
		g.drawLine(x + 2, y - 2, x - 2, y + 2);
	}
}