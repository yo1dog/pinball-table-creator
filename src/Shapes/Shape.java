package Shapes;
public abstract class Shape
{
	protected java.awt.Color c;
	protected boolean selected = false;
	
	public abstract void Draw(java.awt.Graphics g);
	public abstract java.awt.Point GetClosestPoint(int x, int y);
	public abstract boolean GetRectCollision(int rx1, int ry1, int rx2, int ry2);
	
	public final void Select()
	{
		selected = true;
	}
	public final void Deselect()
	{
		selected = false;
	}
}
