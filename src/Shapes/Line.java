package Shapes;
import Default.G;

public final class Line extends Shape
{
	int x1, y1, x2, y2;
	int nx1, ny1, nx2, ny2;
	double friction;
	double damping;
	
	public Line(int x1, int y1, int x2, int y2)
	{
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;
		
		double m = Math.sqrt((x2 - x1)*(x2 - x1) + (y2 - y1)*(y2 - y1));
		this.nx1 = x1 + (x2 - x1) / 2;
		this.ny1 = y1 + (y2 - y1) / 2;
		this.nx2 = (int)(nx1 + (y1 - y2) * 20 / m);
		this.ny2 = (int)(ny1 + (x2 - x1) * 20 / m);
		
		c = java.awt.Color.blue;
	}
	
	public java.awt.Point GetClosestPoint(int bx, int by)
	{
		double d1 = Math.sqrt((bx - x1)*(bx - x1) + (by - y1)*(by - y1));
		double d2 = Math.sqrt((bx - x2)*(bx - x2) + (by - y2)*(by - y2));
		double d3 = Math.sqrt((bx - nx1)*(bx - nx1) + (by - ny1)*(by - ny1));
		
		if (d1 < d2 && d1 < d3)
			return new java.awt.Point(x1, y1);
		else if (d2 < d1 && d2 < d3)
			return new java.awt.Point(x2, y2);
		else if (d3 < d1 && d3 < d2)
			return new java.awt.Point(nx1, ny1);
		else if (d1 == d2 && d1 < d3)
			return new java.awt.Point(x1, y1);
		else if (d1 == d3 && d1 < d2)
			return new java.awt.Point(x1, y1);
		else
			return new java.awt.Point(x2, y2);
	}
	
	public boolean GetRectCollision(int rx1, int ry1, int rx2, int ry2)
	{
		if (x1 > rx1 && x1 < rx2 && y1 > ry1 && y1 < ry2)
			return true;
		else if (x2 > rx1 && x2 < rx2 && y2 > ry1 && y2 < ry2)
			return true;
		else
			return false;
	}
	
	public void Draw(java.awt.Graphics g)
	{
		if (selected)
			g.setColor(java.awt.Color.green);
		else
			g.setColor(c);
		
		g.drawLine(x1, y1, x2, y2);
		G.drawArrow(nx1, ny1, nx2, ny2, 5, g);
	}
}
