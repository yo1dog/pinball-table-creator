package Shapes;
public final class Circle extends Shape
{
	int x, y, r, d;
	double friction;
	double damping;
	
	public Circle(int x, int y, int r)
	{
		this.x = x;
		this.y = y;
		this.r = r;
		d = 2*r;
		
		c = java.awt.Color.blue;
	}

	public java.awt.Point GetClosestPoint(int bx, int by)
	{
		double d = Math.sqrt((bx - x)*(bx - x) + (by - y)*(by - y));
		return new java.awt.Point(x + (int)((bx - x)/d * r), y + (int)((by - y)/d * r));
	}
	
	public boolean GetRectCollision(int rx1, int ry1, int rx2, int ry2)
	{
		int width = (rx2 - rx1)/2;
		int height = (rx2 - rx1)/2;
		
	    int circleDistanceX = Math.abs(x - (rx1 + width));
	    int circleDistanceY = Math.abs(y - (ry1 + height));

	    if (circleDistanceX > width + r)
	    	return false;
	    if (circleDistanceY > height + r)
	    	return false;

	    if (circleDistanceX <= width)
	    	return true;
	    if (circleDistanceY <= height)
	    	return true;

	    int cornerDistanceSquared = (circleDistanceX - width)*(circleDistanceX - width) +
	                         (circleDistanceY - height)*(circleDistanceY - height);

	    return (cornerDistanceSquared <= r*r);
	}

	public void Draw(java.awt.Graphics g)
	{
		if (selected)
			g.setColor(java.awt.Color.green);
		else
			g.setColor(c);
		
		g.drawOval(x - r, y - r, d, d);
	}
}
