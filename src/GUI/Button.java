package GUI;
public class Button
{
	public final static int width = 25;
	public final static int height = 23;
	public static java.awt.Image imageSelect;
	public static java.awt.Image imageOver;
	
	public int x, y;
	public java.awt.Image imageIcon;
	
	public Button(int x, int y, java.awt.Image imageIcon)
	{
		this.x = x;
		this.y = y;
		this.imageIcon = imageIcon;
	}
	
	public static void LoadBackgroundImages()
	{
		imageSelect = java.awt.Toolkit.getDefaultToolkit().getImage("GUI/Select.png");
		imageOver = java.awt.Toolkit.getDefaultToolkit().getImage("GUI/Over.png");
	}
}
